//= jquery-ui.min.js
//= particles.min.js
//= ../libs/flickity/flickity.pkgd.min.js

$('.main-slider').flickity({cellAlign:'left',cellSelector: '.main-slider__slide',resize: false,wrapAround: true,arrowShape:{x0:10,x1:60,y1:50,x2:60,y2:40,x3:20}});

$( function() {
	var handle = $( "#custom-handle1" );
	var sliderrange1 = $( "#slider1" );
	var start = {
		min: 2,
		max: 15,
		value: 5
	};
	$( "#slider1" ).slider({
		min: start.min,
		max: start.max,
		value: start.value,
		create: function() {
			handle.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			handle.text( ui.value );
			sliderrange1.css('background-image', ' linear-gradient(to right, rgb(0, 168, 197) 0% , rgb(0, 168, 197) 0% ,  #84d4f5 ' + ((ui.value - start.min) / (start.max - start.min) * 100) + '% , transparent ' + ((ui.value - start.min) / (start.max - start.min) * 100)  + '%)')
		}
	});
} );

$( function() {
	var handle = $( "#custom-handle2" );
	var sliderrange2 = $( "#slider2" );
	var start = {
		min: 30,
		max: 100,
		value: 77
	};
	$( "#slider2" ).slider({
		min: start.min,
		max: start.max,
		value: start.value,
		create: function() {
			handle.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			handle.text( ui.value );
			sliderrange2.css('background-image', ' linear-gradient(to right, rgb(0, 168, 197) 0% , rgb(0, 168, 197) 0% ,  #84d4f5 ' + ((ui.value - start.min) / (start.max - start.min) * 100) + '% , transparent ' + ((ui.value - start.min) / (start.max - start.min) * 100)  + '%)')
		}
	});
} );

var $responds = $('.responds__slider').flickity({
	cellAlign: 'center',
	initialIndex: 0,
	pageDots: false,
	wrapAround: true,
	adaptiveHeight: true,
	percentPosition: false,
	arrowShape: {x0: 10, x1: 60, y1: 50, x2: 60, y2: 40, x3: 20}
})

var flkty = $responds.data('flickity');
if (flkty) {
	$('#count').text(flkty.slides.length);
	$responds.on( 'select.flickity', function(e) {
		$('#current').text(flkty.selectedIndex+1);
	});
}

var $blog = $('.blog-list__slider').flickity({
	cellAlign: 'left',
	initialIndex: 0,
	pageDots: false,
	wrapAround: true,
	adaptiveHeight: true,
	percentPosition: false,
	arrowShape: {x0: 10, x1: 60, y1: 50, x2: 60, y2: 40, x3: 20}
})

// sandwich menu

$('#sandwich').click(function(){
	$(this).closest('body').toggleClass('body__active');
});


//owl help


var $owl = $('.js-bird'), $owlHelp = $('.js-bird__help');
var help = ['Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, dicta!', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio architecto ipsam dignissimos accusamus mollitia incidunt quis amet hic impedit in fugiat corrupti non, dolorum, adipisci explicabo rem sit, quasi omnis.', 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste voluptatibus ex perspiciatis laborum? Similique, natus.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium.',];


$owl.on('mouseover', function () {
	var numberHelp = Math.round(Math.random() * (help.length-1));
	$(this).append('<div class="js-bird__help"> ' + help[numberHelp] + ' </div>');

});
$owl.on('mouseout', function () {
	$('.js-bird__help').remove();
});


particlesJS.load('particles-js', 'particles/particles.json', function() {
	console.log('callback - particles.js config loaded');
});

$('.sandwich-down').click(function(){
	$(this).find('.drop-down-sandwich-menu').toggleClass('drop-down-sandwich-menu--active');
});

