'use strict';

var gulp = require('gulp'),
watch = require('gulp-watch'),
prefixer = require('gulp-autoprefixer'),
uglify = require('gulp-uglify'),
sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
rigger = require('gulp-rigger'),
cssmin = require('gulp-minify-css'),
imagemin = require('gulp-imagemin'),
pngquant = require('imagemin-pngquant'),
rimraf = require('rimraf'),
browserSync = require("browser-sync"),
criticalCss = require('gulp-penthouse'),
reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/',
        libs: 'build/libs/',
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/main.js',
        style: 'src/style/main.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        libs: ['src/libs/**/*.*','bower_components/**/*.*']
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: ['src/style/**/*.scss','bower_components/bootstrap/scss/**/*.scss'],
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        libs: ['src/libs/**/*.*','bower_components/**/*.*']
    },
    clean: './build'
};

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});
gulp.task('critical-css', function () {
    return gulp.src('build/css/main.css')
        .pipe(criticalCss({
            out: 'critical.css',
            url: 'build/index.html',
            width: 1300,
            height: 900,
            strict: true,
            userAgent: 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
        }))
        .pipe(cssmin())
        .pipe(gulp.dest('build/css/'));
});
gulp.task('html:build', function () {
    gulp.src(path.src.html) 
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) 
    .pipe(rigger()) 
    //.pipe(sourcemaps.init()) 
    .pipe(uglify()) 
    //.pipe(sourcemaps.write()) 
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) 
    .pipe(sass({
        includePaths: [path.src.style,'bower_components/bootstrap/scss/**/*.scss'],
        outputStyle: 'expanded',
        sourceMap: true,
        errLogToConsole: true
    }).on('error', sass.logError))
    .pipe(prefixer({browsers: ['last 4 versions']}))
    .pipe(cssmin())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) 
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()],
        interlaced: true
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});
gulp.task('libs:build', function() {
    gulp.src(path.src.libs)
    .pipe(gulp.dest(path.build.libs))
});
gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build',
    'libs:build'
    ]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch(path.watch.style, function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });


});


gulp.task('default', ['build', 'watch'], function(){
    browserSync({
        server: {
            baseDir: "./build"
        },
        tunnel: true,
        host: 'localhost',
        port: 9000
    });
});